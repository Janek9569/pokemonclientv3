#pragma once
#include "SFML\Network.hpp"
#include "DebugTools.h"
#include "TCPEnums.h"
#include <string>

class ToClientPacketDataContainer
{
public:
	ToClientPacketDataContainer() : cmd(COMMAND_TO_CLIENT_LAST) {}

	virtual sf::Packet& extractData(sf::Packet& pack);
	virtual std::string getName() const = 0;

	CommandsToClient getCmd() const;
private:
	CommandsToClient cmd;
};