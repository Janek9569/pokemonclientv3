#include "PacketHandler.h"

PacketHandler::PacketHandler(sf::IpAddress serverIp, int port):
	tcpClient(serverIp, port),
	imStillThereThread(&PacketHandler::imStillThereTask, this)
{
	constexpr uint8_t MAX_ATTEMPTS = 5;
	uint8_t attempts = 0;

	while(!tcpClient.isConnected())
	{
		if (attempts++ >= MAX_ATTEMPTS)
		{
			dbg_printf("Could not connect to the server\n");
			return;
		}
		tcpClient.connect(serverIp, port);
	}
	ASSERT(tcpClient.isConnected() == true);
	imStillThereThread.launch();
}


PacketHandler::~PacketHandler()
{
}

bool PacketHandler::sendPacket(CommandsToServer cmd, const ToServerPacketDataContainer& container)
{
	ASSERT(tcpClient.isConnected());
	ASSERT(cmd < COMMAND_TO_SERVER_LAST);
	dbg_printf("Sending command to server %s\n", enum2str(cmd));
	sf::Packet pack;
	pack << cmd;
	container.injectData(pack);
	if (!tcpClient.send(pack))
	{
		dbg_printf("packet sending failed while command %s", enum2str(cmd));
		return false;
	}
	return true;
}

CommandsToClient PacketHandler::receivePacket(ToClientPacketDataContainer& container)
{
	ASSERT(tcpClient.isConnected());
	sf::Packet pack;

	if (!tcpClient.receive(pack))
	{
		dbg_printf("packet receiving failed while container filling %s\n", container.getName().c_str());
		return COMMAND_TO_CLIENT_LAST;
	}

	container.extractData(pack);
	return container.getCmd();
}

bool PacketHandler::sendAndReceivePackets(CommandsToServer cmd, const ToServerPacketDataContainer& srvCont,
	CommandsToClient& clientCmd, ToClientPacketDataContainer& cliCont)
{
	ASSERT(tcpClient.isConnected());
	sendPacket(cmd, srvCont);
	return (receivePacket(cliCont) < COMMAND_TO_CLIENT_LAST);
}

void PacketHandler::imStillThereTask()
{
	while (true)
	{
		if (tcpClient.isConnected())
		{
			ImStillThereContainer container;
			sendPacket(IM_STILL_THERE, container);
		}
		sf::sleep(IM_STILL_THERE_PERIOD);
	}
}