#pragma once
#include "TCPClient.h"
#include "ToServerPacketContainers.h"
#include "ToClientPacketDataContainers.h"
#include "TCPEnums.h"

#define IM_STILL_THERE_PERIOD sf::seconds(20)

class PacketHandler
{
public:
	PacketHandler(sf::IpAddress serverIp, int port);
	~PacketHandler();

	bool sendAndReceivePackets(CommandsToServer cmd, const ToServerPacketDataContainer& srvCont,
		CommandsToClient& clientCmd, ToClientPacketDataContainer& cliCont);

	bool isConnected() const { return tcpClient.isConnected(); }
private:
	TCPClient tcpClient;
	sf::Thread imStillThereThread;
	void imStillThereTask();

	bool sendPacket(CommandsToServer cmd, const ToServerPacketDataContainer& container);
	CommandsToClient receivePacket(ToClientPacketDataContainer& container);
};

