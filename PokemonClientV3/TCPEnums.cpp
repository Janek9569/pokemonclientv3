#include "TCPEnums.h"
#include "SFML\Network.hpp"

sf::Packet& operator >> (sf::Packet& pack, CommandsToServer& cmd)
{
	uint32_t cmd_data;
	pack >> cmd_data;
	cmd = static_cast<CommandsToServer>(cmd_data);
	return pack;
}

sf::Packet& operator << (sf::Packet& pack, CommandsToServer cmd)
{
	uint32_t data = static_cast<CommandsToServer>(cmd);
	pack << data;
	return pack;
}

sf::Packet& operator >> (sf::Packet& pack, CommandsToClient& cmd)
{
	uint32_t cmd_data;
	pack >> cmd_data;
	cmd = static_cast<CommandsToClient>(cmd_data);
	return pack;
}

sf::Packet& operator << (sf::Packet& pack, CommandsToClient cmd)
{
	uint32_t data = static_cast<CommandsToClient>(cmd);
	pack << data;
	return pack;
}