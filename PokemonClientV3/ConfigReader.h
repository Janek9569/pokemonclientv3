#pragma once
#include <string>
#include "XMLUtils.h"
#include "Defines.h"

class ConfigReader
{
public:
	ConfigReader(const std::string& configFileName = CLIENT_CONFIG_FILE);
	~ConfigReader();

	int getTCPPort() const;
	std::string getServerIp() const;

private:
	XMLDoc doc;
	XMLNode* root;
};