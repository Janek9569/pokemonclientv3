#include "TCPClient.h"
#include "DebugTools.h"


TCPClient::TCPClient(const sf::IpAddress& addr, uint32_t port):
	_isConnected(false)
{
	if (socket.connect(addr, port, CONNECTION_TIMEOUT) != sf::Socket::Done)
	{
		dbg_printf("Timeout while connecting to the server\n");
		_isConnected = false;
	}

	_isConnected = true;
}


TCPClient::~TCPClient()
{
}

bool TCPClient::connect(const sf::IpAddress& addr, uint32_t port)
{
	if (socket.connect(addr, port, CONNECTION_TIMEOUT) != sf::Socket::Done)
	{
		dbg_printf("Timeout while connecting to the server\n");
		_isConnected = false;
		return false;
	}
	_isConnected = true;
	return true;
}

bool TCPClient::send(sf::Packet& pack)
{
	sf::Lock lock(mutex);
	if (socket.send(pack) != sf::Socket::Done)
	{
		_isConnected = false;
		dbg_printf("Packet sending failed\n");
		return false;
	}
	return true;
}

bool TCPClient::receive(sf::Packet& pack)
{
	sf::Lock lock(mutex);
	if (socket.receive(pack) != sf::Socket::Done)
	{
		_isConnected = false;
		dbg_printf("Packet sending failed\n");
		return false;
	}
	return true;
}

size_t TCPClient::send(const void* data, size_t len)
{
	size_t sent = 0;
	if (socket.send(data, len, sent) != sf::Socket::Done)
	{
		_isConnected = false;
		dbg_printf("Packet sending failed\n");
		return 0;
	}
	return sent;
}
size_t TCPClient::receive(void* data, size_t len)
{
	size_t recieived = 0;
	if (socket.receive(data, len, recieived) != sf::Socket::Done)
	{
		_isConnected = false;
		dbg_printf("Packet sending failed\n");
		return 0;
	}
	return recieived;
}