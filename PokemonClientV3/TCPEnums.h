#pragma once
#include "SFML\Network.hpp"
#include "Utils.h"
#include "DebugTools.h"

DEFINE_ENUM_WITH_STRING_CONVERSIONS(CommandsToServer,
(NONE_SERVER_COMMAND)
(MOVE)
(LOGIN)
(LOGOUT)
(GET_CHARACTER_LIST)
(DISCONNECT)
(DISCONNECT_ACCOUNT)
(IM_STILL_THERE)
(GET_CHUNK)
(COMMAND_TO_SERVER_LAST)
)

DEFINE_ENUM_WITH_STRING_CONVERSIONS(CommandsToClient,
(PASSWORD_CORRECT)
(INCORRECT_PASSORD)
(ALLOW_TO_LOG_IN)
(ALLOW_TO_LOG_OUT)
(STOP_LOGGING_IN)
(PLAYER_ALREDY_LOGGED_IN)
(ACC_ALREDY_CONNECTED)
(COMMAND_TO_CLIENT_LAST)
)

sf::Packet& operator >> (sf::Packet& pack, CommandsToServer& cmd);
sf::Packet& operator << (sf::Packet& pack, CommandsToServer cmd);
sf::Packet& operator >> (sf::Packet& pack, CommandsToClient& cmd);
sf::Packet& operator << (sf::Packet& pack, CommandsToClient cmd);