#pragma once
#include <stdarg.h>
#include <stdio.h>
#include <cstdint>

constexpr uint32_t DBG_BUFFER_LEN = 500;
void dbg_printf(const char *fmt, ...);