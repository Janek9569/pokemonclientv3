#include <TGUI\TGUI.hpp>
#include "PacketHandler.h"
#include "ConfigReader.h"

int main()
{
	sf::RenderWindow window{ { 800, 600 }, "Window" };
	tgui::Gui gui{ window }; // Create the gui and attach it to the window

	ConfigReader cfg;
	PacketHandler packetHandler(cfg.getServerIp(), cfg.getTCPPort());


	while (window.isOpen())
	{
		sf::Event event;
		while (window.pollEvent(event))
		{
			if (event.type == sf::Event::Closed)
				window.close();

			gui.handleEvent(event); // Pass the event to the widgets
		}

		window.clear();
		gui.draw(); // Draw all widgets
		window.display();
	}
}