#include "ToClientPacketDataContainers.h"
#include "ToServerPacketContainers.h"

sf::Packet& ToClientPacketDataContainer::extractData(sf::Packet& pack)
{
	dbg_printf("Default pack extracting?!\n");
	return pack;
}

CommandsToClient ToClientPacketDataContainer::getCmd() const 
{ 
	return cmd; 
}

sf::Packet& ToServerPacketDataContainer::injectData(sf::Packet& pack) const
{
	dbg_printf("Default pack sending?!\n");
	pack << "Default pack sending?!\n";
	return pack;
}

CommandsToServer ToServerPacketDataContainer::getCmd() const
{ 
	return cmd; 
}

sf::Packet& ImStillThereContainer::injectData(sf::Packet& pack)
{
	dbg_printf("ImStillThere sending\n");
	return pack;
}

std::string ImStillThereContainer::getName() const
{
	return "ImStillThereContainer";
}