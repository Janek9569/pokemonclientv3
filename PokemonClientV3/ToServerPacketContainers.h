#pragma once
#include "SFML\Network.hpp"
#include "DebugTools.h"
#include "TCPEnums.h"

class ToServerPacketDataContainer
{
public:
	ToServerPacketDataContainer() : cmd(COMMAND_TO_SERVER_LAST) {}

	virtual sf::Packet& injectData(sf::Packet& pack) const;
	virtual std::string getName() const = 0;

	CommandsToServer getCmd() const;
private:
	CommandsToServer cmd;
};

class ImStillThereContainer : public ToServerPacketDataContainer
{
public:
	virtual sf::Packet& injectData(sf::Packet& pack);
	virtual std::string getName() const;
};