#pragma once

#include <boost/preprocessor.hpp>

#define X_DEFINE_ENUM_WITH_STRING_CONVERSIONS_TOSTRING_CASE(r, data, elem)    \
    case elem : return BOOST_PP_STRINGIZE(elem);

#define DEFINE_ENUM_WITH_STRING_CONVERSIONS(name, enumerators)                \
    enum name {                                                               \
        BOOST_PP_SEQ_ENUM(enumerators)                                        \
    };                                                                        \
                                                                              \
    inline const char* enum2str(name v)                                       \
    {                                                                         \
        switch (v)                                                            \
        {                                                                     \
            BOOST_PP_SEQ_FOR_EACH(                                            \
                X_DEFINE_ENUM_WITH_STRING_CONVERSIONS_TOSTRING_CASE,          \
                name,                                                         \
                enumerators                                                   \
            )                                                                 \
            default: return "[Unknown " BOOST_PP_STRINGIZE(name) "]";         \
        }                                                                     \
    }

#define ERR_BUFFER_SIZE 500
extern char err_msg_buffer[ERR_BUFFER_SIZE];

#define STR(x) #x

inline void error_handler()
{
	while (true)
	{

	}
}

#define ASSERT(x)																			\
do{																							\
	if(!(x))																				\
	{																						\
		snprintf(err_msg_buffer, ERR_BUFFER_SIZE, 											\
		"ASSERTION [ %s ] in function %s, in line %d", STR(x), __FUNCTION__, __LINE__ );	\
		error_handler();																	\
	}																						\
}while(false)