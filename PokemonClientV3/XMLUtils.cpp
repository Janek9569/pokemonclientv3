#include "XMLUtils.h"
#include "DebugTools.h"
#include <iostream>
#include <sstream>

bool parseDocument(XMLDoc& doc, std::string dir)
{
	static std::string content;
	std::ifstream file(dir);
	if (!file.good())
	{
		std::cerr << "Can not read file " << dir << std::endl;
		return false;
	}
	std::stringstream buffer;
	buffer << file.rdbuf();
	file.close();
	content = buffer.str();
	try 
	{ 
		doc.parse<0>(&content[0]); 
	}
	catch (rapidxml::parse_error p)
	{
		std::cerr << "Can not parse xml file " << dir << std::endl;
		return false;
	}
	return true;
}
