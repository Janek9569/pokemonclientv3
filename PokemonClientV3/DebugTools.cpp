#include "DebugTools.h"

void dbg_printf(const char *fmt, ...)
{
#ifdef _DEBUG
	va_list args;
	va_start(args, fmt);
	vfprintf(stderr, fmt, args);
	va_end(args);
#else
	(void)fmt;
#endif
}