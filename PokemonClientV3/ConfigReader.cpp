#include "ConfigReader.h"
#include "DebugTools.h"
#include "Utils.h"

ConfigReader::ConfigReader(const std::string& configFileName)
{
	dbg_printf("Loading client config File\n");

	if (!parseDocument(doc, configFileName))
	{
		dbg_printf("Client config file not parsed\n");
		exit(1);
	}

	root = doc.first_node("clientConfig");
	if (!root)
	{
		dbg_printf("Error: can not find clientConfig node in config file\n");
		exit(1);
	}
}

ConfigReader::~ConfigReader()
{
}

int ConfigReader::getTCPPort() const
{
	ASSERT(root != 0);

	XMLNode *TCPRoot = root->first_node("TCP");
	if (!TCPRoot)
	{
		dbg_printf("Error: can not find TCP node in the config file\n");
		exit(1);
	}

	uint32_t port = atoi(TCPRoot->first_attribute("port")->value());
	dbg_printf("TCPPort read := %d\n", port);

	return port;
}

std::string ConfigReader::getServerIp() const
{
	ASSERT(root != 0);

	XMLNode *TCPRoot = root->first_node("TCP");
	if (!TCPRoot)
	{
		dbg_printf("Error: can not find TCP node in the config file\n");
		exit(1);
	}
	std::string serverIp = TCPRoot->first_attribute("serverIp")->value();
	dbg_printf("ServerIp read := %s\n", serverIp.c_str());

	return serverIp;
}

