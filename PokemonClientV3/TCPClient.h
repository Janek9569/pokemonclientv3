#pragma once
#include "SFML\Network.hpp"
#include <cstdint>

#define CONNECTION_TIMEOUT sf::seconds(20)

class TCPClient : sf::NonCopyable
{
public:
	TCPClient(const sf::IpAddress& addr, uint32_t port);
	~TCPClient();

	bool connect(const sf::IpAddress& addr, uint32_t port);
	bool isConnected() const { return _isConnected; }
	bool send(sf::Packet& pack);
	bool receive(sf::Packet& pack);
	size_t send(const void* data, size_t len);
	size_t receive(void* data, size_t len);
private:
	sf::Mutex mutex;
	bool _isConnected;
	sf::TcpSocket socket;
};

